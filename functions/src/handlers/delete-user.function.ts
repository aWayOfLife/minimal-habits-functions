import { UserRecord } from 'firebase-admin/auth';
import * as functions from 'firebase-functions';

export const deleteUser = functions.auth.user().onDelete((user: UserRecord) => {
  console.log('Initiating User Account deletion execution', user);
  console.log('Completing User Account deletion execution');
});
