import { UserRecord } from 'firebase-admin/auth';
import * as functions from 'firebase-functions';
import { addUserAsync } from '../services/add-user.service.js';

export const addUser = functions.auth.user().onCreate((user: UserRecord) => {
  console.log('Initiating User Account addition execution', user);
  addUserAsync(user)
    .then(({ result }) => console.log('User Account added successfully', result))
    .catch((error) => console.log('Error while adding User Account', error))
    .finally(() => console.log('Completing User Account addtion execution'));
});
