import { UserRecord } from 'firebase-admin/auth';
import { AccountDto } from '../shared/dtos/account.dto.js';
import { makeRequest } from '../shared/helpers/others/make-request.js';
import { login } from '../shared/helpers/others/login-request.js';

export const addUserAsync = async (user: UserRecord) => {
  const { cookie } = await login();
  const url = process.env.BASE_URL + '/api/account';
  const accountDto: AccountDto = {
    accountId: user.uid,
    name: user.displayName ?? 'Anonymous',
    email: user.email,
  };
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Cookie: cookie,
    },
    body: JSON.stringify(accountDto),
  };
  return await makeRequest(url, options);
};
