import { signInWithEmailAndPassword } from 'firebase/auth';
import { firebaseConfig, getFirebaseApp, getFirebaseAuth } from './get-auth.js';

export const getToken = async () => {
  try {
    const email = process.env.EMAIL;
    const password = process.env.PASSWORD;
    const app = getFirebaseApp(firebaseConfig);
    const auth = getFirebaseAuth(app);
    await signInWithEmailAndPassword(auth, email, password);
    const token = (await auth.currentUser.getIdTokenResult()).token;
    return token;
  } catch (error) {
    console.log('Error while sign in using email and password', error);
    throw error;
  }
};
