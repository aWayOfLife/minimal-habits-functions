import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getConfig } from './get-firebase-config.js';

export const firebaseConfig = getConfig();
export const getFirebaseApp = (config) => initializeApp(config);
export const getFirebaseAuth = (app) => getAuth(app);
