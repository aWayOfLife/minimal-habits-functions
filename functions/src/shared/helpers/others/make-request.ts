export const makeRequest = async (url: string, options: any) => {
  try {
    console.log('Making Request to', url);
    console.log('With options', options);
    const timeoutDuration = 10000;
    const controller = new AbortController();
    const timeoutId = setTimeout(() => {
      controller.abort();
    }, timeoutDuration);

    options = {
      ...options,
      signal: controller.signal,
    };

    const response = await fetch(url, options);

    clearTimeout(timeoutId);

    if (!response.ok) {
      const errorMessage = await response.text();
      throw new Error(errorMessage);
    }
    const cookie = response.headers.get('Set-Cookie');
    const result = await response.json();
    return { result, cookie };
  } catch (error) {
    console.log(`Error while making request to ${url}`);
    throw error;
  }
};
