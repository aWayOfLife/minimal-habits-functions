import { LoginDto } from '../../dtos/login.dto.js';
import { getToken } from '../firebase/get-token.js';
import { makeRequest } from './make-request.js';

export const login = async () => {
  const token = await getToken();
  const url = process.env.BASE_URL + '/api/auth/login';
  const loginDto: LoginDto = {
    idToken: token,
  };
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(loginDto),
  };
  return await makeRequest(url, options);
};
