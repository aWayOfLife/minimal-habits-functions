export interface IAccount {
  accountId: string;
  name: string;
  email: string;
}

export class AccountDto implements IAccount {
  accountId: string;
  name: string;
  email: string;
}
