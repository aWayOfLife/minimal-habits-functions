export interface ILogin {
  idToken: string;
}

export class LoginDto implements ILogin {
  idToken: string;
}
